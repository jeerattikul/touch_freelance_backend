package admin

import (
	"go-template-training/service/admin"
)

type Controller struct {
	service admin.Service
}

func New(adminSvc admin.Service) (admin *Controller) {
	return &Controller{service: adminSvc}
}
