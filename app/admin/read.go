package admin

import (
	"go-template-training/app/view"
	"go-template-training/service/admin/inout"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// Read godoc
// @Tags Admins
// @Summary Get a admin by admin ID
// @Description Response a admin data with a given admin ID
// @Accept json
// @Produce json
// @Param Accept-Language header string false "Language" default(en)
// @Param id path string true "Admin ID" default(123456789012345678)
// @Success 200 {object} view.SuccessReadResp{data=inout.AdminView}
// @Failure 400 {object} view.Error400Resp{errors=[]view.ErrItem}
// @Failure 404 {object} view.Error404Resp{errors=[]view.ErrItem}
// @Failure 422 {object} view.Error422Resp{errors=[]view.ErrItem}
// @Failure 500 {object} view.Error500Resp{errors=[]view.ErrItem}
// @Router /admins/{id} [get]
func (ctrl *Controller) Read(c *gin.Context) {
	sp, ctx := opentracing.StartSpanFromContext(c, "handler.admin.Read")
	defer sp.Finish()

	inp := &inout.AdminReadInput{
		ID: c.Param("id"),
	}

	admin, err := ctrl.service.Read(ctx, inp)
	if err != nil {
		view.MakeErrResp(c, err)
		return
	}

	view.MakeReadSuccessResp(c, admin)
}
