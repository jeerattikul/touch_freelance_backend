package app

import (
	"context"

	"go-template-training/app/admin"
	"go-template-training/app/menus"
	"go-template-training/app/outsource"
	"go-template-training/app/project"
	"go-template-training/app/staff"

	"go-template-training/config"
	"go-template-training/repository/mongodb"
	repoRedis "go-template-training/repository/redis"

	serviceAdmin "go-template-training/service/admin/implement"
	serviceMenus "go-template-training/service/menus/implement"
	serviceOutsource "go-template-training/service/outsource/implement"
	serviceProject "go-template-training/service/project/implement"
	serviceStaff "go-template-training/service/staff/implement"

	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	serviceValidator "go-template-training/service/validator"
)

type App struct {
	staff     *staff.Controller
	admin     *admin.Controller
	project   *project.Controller
	outsource *outsource.Controller
	menus     *menus.Controller
}

const (
	collStaff     = "staffs"
	collAdmin     = "admins"
	collProject   = "projects"
	collOutsource = "outsources"
	collMenu      = "menus"
)

func newApp(appConfig *config.Config, log logs.Log) *App {
	ctx := context.Background()
	uuid, err := util.NewUUID()
	panicIfErr(log, err)
	datetime := util.NewDateTime(appConfig)
	filterString := util.NewFilterString()

	// repositories
	redisRepo, err := repoRedis.New(ctx, appConfig)
	panicIfErr(log, err)
	staffRepo, err := mongodb.New(ctx, mongoDBConfig(appConfig, collStaff))
	panicIfErr(log, err)
	adminRepo, err := mongodb.New(ctx, mongoDBConfig(appConfig, collAdmin))
	panicIfErr(log, err)
	projectRepo, err := mongodb.New(ctx, mongoDBConfig(appConfig, collProject))
	panicIfErr(log, err)
	outsourceRepo, err := mongodb.New(ctx, mongoDBConfig(appConfig, collOutsource))
	panicIfErr(log, err)
	menusRepo, err := mongodb.New(ctx, mongoDBConfig(appConfig, collMenu))
	panicIfErr(log, err)

	// validators
	validator := serviceValidator.New(&serviceValidator.ValidatorRepository{
		Staff:     staffRepo,
		Admin:     adminRepo,
		Project:   projectRepo,
		Outsource: outsourceRepo,
	})

	// services
	staffService := serviceStaff.New(&serviceStaff.StaffServiceConfig{
		Validator:    validator,
		RepoStaff:    staffRepo,
		RepoRedis:    redisRepo,
		UUID:         uuid,
		DateTime:     datetime,
		FilterString: filterString,
		Log:          log,
	})
	adminService := serviceAdmin.New(&serviceAdmin.AdminServiceConfig{
		Validator:    validator,
		RepoAdmin:    adminRepo,
		RepoRedis:    redisRepo,
		UUID:         uuid,
		DateTime:     datetime,
		FilterString: filterString,
		Log:          log,
	})
	projectService := serviceProject.New(&serviceProject.ProjectServiceConfig{
		Validator:    validator,
		RepoProject:  projectRepo,
		RepoRedis:    redisRepo,
		UUID:         uuid,
		DateTime:     datetime,
		FilterString: filterString,
		Log:          log,
	})
	outsourceService := serviceOutsource.New(&serviceOutsource.OutsourceServiceConfig{
		Validator:     validator,
		RepoOutsource: outsourceRepo,
		RepoRedis:     redisRepo,
		UUID:          uuid,
		DateTime:      datetime,
		FilterString:  filterString,
		Log:           log,
	})
	menuService := serviceMenus.New(&serviceMenus.MenusServiceConfig{
		Validator:    validator,
		Repo:         menusRepo,
		UUID:         uuid,
		DateTime:     datetime,
		FilterString: filterString,
		Log:          log,
	})

	return &App{
		staff:     staff.New(staffService),
		admin:     admin.New(adminService),
		project:   project.New(projectService),
		outsource: outsource.New(outsourceService),
		menus:     menus.New(menuService),
	}
}

func mongoDBConfig(appConfig *config.Config, collName string) *mongodb.Config {
	return &mongodb.Config{
		URI:      appConfig.MongoDBEndpoint,
		DBName:   appConfig.MongoDBName,
		CollName: collName,
		Timeout:  appConfig.MongoDBTimeout,
	}
}

func panicIfErr(log logs.Log, err error) {
	if err != nil {
		log.Panic(err)
	}
}
