package outsource

import (
	"go-template-training/service/outsource"
)

type Controller struct {
	service outsource.Service
}

func New(outsourceSvc outsource.Service) (outsource *Controller) {
	return &Controller{service: outsourceSvc}
}
