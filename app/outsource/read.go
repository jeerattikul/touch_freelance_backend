package outsource

import (
	"go-template-training/app/view"
	"go-template-training/service/outsource/inout"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// Read godoc
// @Tags Outsources
// @Summary Get a outsource by outsource ID
// @Description Response a outsource data with a given outsource ID
// @Accept json
// @Produce json
// @Param Accept-Language header string false "Language" default(en)
// @Param id path string true "Outsource ID" default(123456789012345678)
// @Success 200 {object} view.SuccessReadResp{data=inout.OutsourceView}
// @Failure 400 {object} view.Error400Resp{errors=[]view.ErrItem}
// @Failure 404 {object} view.Error404Resp{errors=[]view.ErrItem}
// @Failure 422 {object} view.Error422Resp{errors=[]view.ErrItem}
// @Failure 500 {object} view.Error500Resp{errors=[]view.ErrItem}
// @Router /outsources/{id} [get]
func (ctrl *Controller) Read(c *gin.Context) {
	sp, ctx := opentracing.StartSpanFromContext(c, "handler.outsource.Read")
	defer sp.Finish()

	inp := &inout.OutsourceReadInput{
		ID: c.Param("id"),
	}

	outsource, err := ctrl.service.Read(ctx, inp)
	if err != nil {
		view.MakeErrResp(c, err)
		return
	}

	view.MakeReadSuccessResp(c, outsource)
}
