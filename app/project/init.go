package project

import (
	"go-template-training/service/project"
)

type Controller struct {
	service project.Service
}

func New(projectSvc project.Service) (project *Controller) {
	return &Controller{service: projectSvc}
}
