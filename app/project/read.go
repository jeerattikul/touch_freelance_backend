package project

import (
	"go-template-training/app/view"
	"go-template-training/service/project/inout"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
)

// Read godoc
// @Tags Projects
// @Summary Get a project by project ID
// @Description Response a project data with a given project ID
// @Accept json
// @Produce json
// @Param Accept-Language header string false "Language" default(en)
// @Param id path string true "Project ID" default(123456789012345678)
// @Success 200 {object} view.SuccessReadResp{data=inout.ProjectView}
// @Failure 400 {object} view.Error400Resp{errors=[]view.ErrItem}
// @Failure 404 {object} view.Error404Resp{errors=[]view.ErrItem}
// @Failure 422 {object} view.Error422Resp{errors=[]view.ErrItem}
// @Failure 500 {object} view.Error500Resp{errors=[]view.ErrItem}
// @Router /projects/{id} [get]
func (ctrl *Controller) Read(c *gin.Context) {
	sp, ctx := opentracing.StartSpanFromContext(c, "handler.project.Read")
	defer sp.Finish()

	inp := &inout.ProjectReadInput{
		ID: c.Param("id"),
	}

	project, err := ctrl.service.Read(ctx, inp)
	if err != nil {
		view.MakeErrResp(c, err)
		return
	}

	view.MakeReadSuccessResp(c, project)
}
