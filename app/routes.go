package app

import (
	"fmt"
	"net/http"
	"os"

	limit "github.com/aviddiviner/gin-limit"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	ginLogRus "github.com/toorop/gin-logrus"

	"go-template-training/config"
	"go-template-training/docs"
	"go-template-training/service/util/logs"
)

type Router struct {
	app       *App
	router    *gin.Engine
	appConfig *config.Config
	log       logs.Log
	logger    *logrus.Logger
}

type Config struct {
	AppConfig *config.Config
	Log       logs.Log
	Logger    *logrus.Logger
}

func New(rc *Config) *Router {
	return &Router{
		app:       newApp(rc.AppConfig, rc.Log),
		router:    gin.New(),
		appConfig: rc.AppConfig,
		log:       rc.Log,
		logger:    rc.Logger,
	}
}

func (r *Router) RegisterRoute() *Router {
	r.routerMiddleware()
	gin.SetMode(r.appConfig.GinMode)

	// api v1
	v1 := r.router.Group(r.appConfig.SwaggerInfoBasePath)
	{
		staff := v1.Group("staffs")
		{
			staff.GET("", r.app.staff.List)
			staff.POST("", r.app.staff.Create)
			staff.GET(":id", r.app.staff.Read)
			staff.PUT(":id", r.app.staff.Update)
			staff.DELETE(":id", r.app.staff.Delete)
		}
		admin := v1.Group("admins")
		{
			admin.GET("", r.app.admin.List)
			admin.POST("", r.app.admin.Create)
			admin.GET(":id", r.app.admin.Read)
			admin.PUT(":id", r.app.admin.Update)
			admin.DELETE(":id", r.app.admin.Delete)
		}
		project := v1.Group("projects")
		{
			project.POST("", r.app.project.Create)
			project.GET(":id", r.app.project.Read)
			project.GET("", r.app.project.List)
			project.DELETE(":id", r.app.project.Delete)
			project.PUT(":id", r.app.project.Update)

		}
		outsource := v1.Group("outsources")
		{
			outsource.POST("", r.app.outsource.Create)
			outsource.GET(":id", r.app.outsource.Read)
			outsource.GET("", r.app.outsource.List)
			outsource.DELETE(":id", r.app.outsource.Delete)
			outsource.PUT(":id", r.app.outsource.Update)

		}
		all := v1.Group("all")
		{
			all.GET("menus", r.app.menus.All)
		}
	}

	r.routeSwagger()
	r.routeHealthCheck()
	return r
}

func (r *Router) Start() {
	if err := r.router.Run(); err != nil {
		r.log.Panic(err)
	}
}

func (r *Router) routerMiddleware() {
	appENV := os.Getenv("APP_ENV")
	if appENV == "local" {
		r.router.Use(r.ginLogWithConfig())
	} else {
		r.router.Use(ginLogRus.Logger(r.logger))
	}
	r.router.Use(r.corsMiddleware())
	r.router.Use(gin.Recovery())
	r.router.Use(limit.MaxAllowed(r.appConfig.AppMaxAllowed))
}

func (r *Router) routeSwagger() {
	// programmatically set swagger info
	docs.SwaggerInfo_swagger.Title = r.appConfig.AppName
	docs.SwaggerInfo_swagger.Description = "API Specification Document"
	docs.SwaggerInfo_swagger.Version = "1.0.0"
	docs.SwaggerInfo_swagger.Host = r.appConfig.SwaggerInfoHost
	docs.SwaggerInfo_swagger.BasePath = r.appConfig.SwaggerInfoBasePath

	docPath := ginSwagger.URL(fmt.Sprintf("//%s/swagger/doc.json", r.appConfig.SwaggerInfoHost))
	r.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, docPath))
}

func (r *Router) routeHealthCheck() {
	r.router.GET("/system/health", func(ctx *gin.Context) { ctx.Status(http.StatusOK) })
}

func (r *Router) ginLogWithConfig() gin.HandlerFunc {
	return gin.LoggerWithConfig(gin.LoggerConfig{
		SkipPaths: []string{
			// "/swagger/index.html",
			"/swagger/swagger-ui.css",
			"/swagger/swagger-ui-standalone-preset.js",
			"/swagger/swagger-ui-bundle.js",
			"/swagger/doc.json",
			"/system/health",
			"/favicon.ico",
		},
	})
}

func (r *Router) corsMiddleware() gin.HandlerFunc {
	corsConf := cors.DefaultConfig()
	corsConf.AllowAllOrigins = true
	corsConf.AllowCredentials = true
	corsConf.AddAllowHeaders("Authorization", "User-Agent")
	return cors.New(corsConf)
}
