package domain

type Admin struct {
	ID        string `bson:"_id,omitempty"`
	UserName  string `bson:"user_name"`
	PassWord  string `bson:"pass_word"`
	FirstName string `bson:"first_name"`
	LastName  string `bson:"last_name"`
	CreatedAt int64  `bson:"created_at"`
	UpdatedAt int64  `bson:"updated_at"`
	DeletedAt int64  `bson:"deleted_atAC,omitempty"`
}
