package domain

type Menu struct {
	ID        string `bson:"_id"`
	KeySlug   string `bson:"key_slug"`
	Name      string `bson:"name"`
	Icon      string `bson:"icon"`
	Sort      int    `bson:"sort"`
	Status    string `bson:"status"`
	CreatedAt int64  `bson:"created_at"`
	UpdatedAt int64  `bson:"updated_at"`
	DeletedAt int64  `bson:"deleted_at,omitempty"`
}
