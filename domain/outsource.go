package domain

type Outsource struct {
	ID           string   `bson:"_id,omitempty"`
	OutName      string   `bson:"out_name"`
	Company      string   `bson:"company"`
	Address      string   `bson:"address"`
	OutPhone     []string `bson:"out_phone"`
	OutFramework []string `bson:"out_framework"`
	OutStatus    string   `bson:"out_status"`
	Note         string   `bson:"note"`
	Project      []string `bson:"project"`
	CreatedAt    int64    `bson:"created_at"`
	UpdatedAt    int64    `bson:"updated_at"`
	DeletedAt    int64    `bson:"deleted_atAC,omitempty"`
}
