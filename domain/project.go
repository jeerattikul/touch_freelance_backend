package domain

type Project struct {
	ID          string   `bson:"_id"`
	ProName     string   `bson:"pro_name"`
	Description string   `bson:"descrip_tion"`
	OutName     string   `bson:"out_name"`
	Price       string   `bson:"price"`
	SoftDel     string   `bson:"soft_del"`
	PerRiod     string   `bson:"per_riod"`
	FrameWork   []string `bson:"frame_work"`
	ProStatus   string   `bson:"pro_status"`
	LinkFile    []string `bson:"link_file"`
	Tel         string   `bson:"tel"`
	CreatedAt   int64    `bson:"created_at"`
	UpdatedAt   int64    `bson:"updated_at"`
	DeletedAt   int64    `bson:"deleted_at,omitempty"`
}
