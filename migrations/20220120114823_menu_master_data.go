package migrations

import (
	"fmt"

	"go-template-training/domain"

	"github.com/uniplaces/carbon"
	migrate "github.com/xakep666/mongo-migrate"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func init() {
	var coll = "menus"
	data := []*domain.Menu{
		{
			KeySlug: "dashboard",
			Name:    "Dashboard",
			Icon:    "dashboard",
			Sort:    1,
			Status:  "active",
		},
		{
			KeySlug: "project",
			Name:    "Project",
			Icon:    "project",
			Sort:    2,
			Status:  "active",
		},
		{
			KeySlug: "outsource",
			Name:    "Outsource",
			Icon:    "outsource",
			Sort:    3,
			Status:  "active",
		},
		{
			KeySlug: "admin",
			Name:    "Admin",
			Icon:    "admin",
			Sort:    4,
			Status:  "active",
		},
	}

	var up migrate.MigrationFunc = func(db *mongo.Database) error {
		for _, v := range data {
			filter := bson.M{"key_slug": v.KeySlug}
			cnt, _ := db.Collection(coll).CountDocuments(ctx, filter)
			if cnt == 0 {
				v.ID = uuid.Generate()
				v.CreatedAt = carbon.Now().Unix()
				v.UpdatedAt = carbon.Now().Unix()
				_, err := db.Collection(coll).InsertOne(ctx, v)
				if err != nil {
					return err
				}
				log.Info(fmt.Sprintf("- Migrate menu \"%s\" success", v.Name))
			}
		}
		return nil
	}
	var down migrate.MigrationFunc = func(db *mongo.Database) error {
		for _, v := range data {
			filter := bson.M{"key_slug": v.KeySlug}
			_, err := db.Collection(coll).DeleteOne(ctx, filter)
			if err != nil {
				return err
			}
			log.Info(fmt.Sprintf("- Rollback menu \"%s\" success", v.Name))
		}
		return nil
	}

	if err := migrate.Register(up, down); err != nil {
		log.Fatal(err)
	}

}
