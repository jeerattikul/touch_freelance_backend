package migrations

import (
	"context"

	"go-template-training/config"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
)

var (
	ctx     = context.Background()
	uuid, _ = util.NewUUID()
	encrypt = util.NewEncrypt()
	log     = logs.New(&config.Config{
		AppLogLevel:       0,
		AppLogEnableColor: true,
	})
)
