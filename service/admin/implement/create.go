package implement

import (
	"context"

	"go-template-training/service/admin/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Create(ctx context.Context, input *inout.AdminCreateInput) (id string, err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return "", util.ValidationCreateErr(err)
	}

	input.ID = impl.UUID.Generate()
	admin := input.ToDomain(impl.DateTime)

	_, err = impl.RepoAdmin.Create(ctx, admin)
	if err != nil {
		return "", util.RepoCreateErr(err)
	}

	return input.ID, nil
}
