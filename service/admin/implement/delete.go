package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/admin/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Delete(ctx context.Context, input *inout.AdminDeleteInput) (err error) {
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoAdmin.Read(ctx, filters, &domain.Admin{}); err != nil {
		return util.RepoReadErr(err)
	}

	if err = impl.RepoAdmin.SoftDelete(ctx, filters); err != nil {
		return util.RepoDeleteErr(err)
	}

	return nil
}
