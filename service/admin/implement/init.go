package implement

import (
	"go-template-training/service/admin"
	wrp "go-template-training/service/admin/wrapper"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	"go-template-training/service/validator"
)

type implementation struct {
	*AdminServiceConfig
}

type AdminServiceConfig struct {
	Validator    validator.Validator
	RepoAdmin    util.Repository
	RepoRedis    util.RepositoryRedis
	UUID         util.UUID
	DateTime     util.DateTime
	FilterString util.FilterString
	Log          logs.Log
}

func New(config *AdminServiceConfig) (service admin.Service) {
	return &wrp.Wrapper{
		Service: &implementation{config},
	}
}
