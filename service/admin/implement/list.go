package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/admin/inout"
	"go-template-training/service/util"
)

func (impl *implementation) List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.AdminView, err error) {
	if err = impl.Validator.Validate(opt); err != nil {
		return 0, nil, util.ValidationParamOptionErr(err)
	}

	total, records, err := impl.RepoAdmin.List(ctx, opt, &domain.Admin{})
	if err != nil {
		return 0, nil, util.RepoListErr(err)
	}

	items = make([]*inout.AdminView, len(records))
	for i, record := range records {
		items[i] = inout.AdminToView(record.(*domain.Admin), impl.DateTime)
	}

	return total, items, nil
}
