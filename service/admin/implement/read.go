package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/admin/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Read(ctx context.Context, input *inout.AdminReadInput) (view *inout.AdminView, err error) {
	admin := &domain.Admin{}
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoAdmin.Read(ctx, filters, admin); err != nil {
		return nil, util.RepoReadErr(err)
	}

	return inout.AdminToView(admin, impl.DateTime), nil
}
