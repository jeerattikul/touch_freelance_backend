package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/admin/inout"
	"go-template-training/service/util"

	"github.com/imdario/mergo"
)

func (impl *implementation) Update(ctx context.Context, input *inout.AdminUpdateInput) (err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return util.ValidationUpdateErr(err)
	}

	admin := &domain.Admin{}
	filters := impl.FilterString.MakeIDFilters(input.ID)

	if err = impl.RepoAdmin.Read(ctx, filters, admin); err != nil {
		return util.RepoReadErr(err)
	}

	update := input.ToDomain(impl.DateTime)

	if err = mergo.Merge(admin, update, mergo.WithOverride); err != nil {
		return util.UnknownErr(err)
	}

	if err = impl.RepoAdmin.Update(ctx, filters, admin); err != nil {
		return util.RepoUpdateErr(err)
	}

	return nil
}
