package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"

	"github.com/modern-go/reflect2"
)

type AdminCreateInput struct {
	ID        string `json:"id" swaggerignore:"true"`
	UserName  string `json:"user_name" validate:"required,not-empty,name-en" example:"non123"`
	PassWord  string `json:"pass_word" example:"abc123456" validate:"not-empty,name-en"`
	FirstName string `json:"first_name" validate:"not-empty" example:"Jeerattikul"`
	LastName  string `json:"last_name" validate:"not-empty" example:"Sanla-ied"`
} // @Name AdminCreateInput

func (input *AdminCreateInput) ToDomain(datetime util.DateTime) (admin *domain.Admin) {
	if reflect2.IsNil(input) {
		return &domain.Admin{}
	}
	return &domain.Admin{
		ID:        input.ID,
		UserName:  input.UserName,
		PassWord:  input.PassWord,
		FirstName: input.FirstName,
		LastName:  input.LastName,
		CreatedAt: datetime.GetUnixNow(),
		UpdatedAt: datetime.GetUnixNow(),
	}
}
