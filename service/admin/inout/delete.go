package inout

type AdminDeleteInput struct {
	ID string `json:"-" validate:"required"`
} // @Name AdminDeleteInput
