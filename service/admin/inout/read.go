package inout

type AdminReadInput struct {
	ID string `json:"-" validate:"required"`
} // @Name AdminReadInput
