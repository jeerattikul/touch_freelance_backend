package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"
)

type AdminView struct {
	ID        string `json:"id" example:"123456789012345678"`
	UserName  string `json:"user_name" example:"John Smith"`
	PassWord  string `json:"pass_word" example:"0900000000"`
	FirstName string `json:"first_name" example:"John"`
	LastName  string `json:"last_name" example:"Smith"`
	CreatedAt string `json:"created_at" example:"2016-01-02 15:04:05"`
	UpdatedAt string `json:"updated_at" example:"2016-01-02 15:04:05"`
} // @Name AdminView

func AdminToView(admin *domain.Admin, datetime util.DateTime) (view *AdminView) {
	return &AdminView{
		ID:        admin.ID,
		UserName:  admin.UserName,
		PassWord:  admin.PassWord,
		FirstName: admin.FirstName,
		LastName:  admin.LastName,
		CreatedAt: datetime.ConvertUnixToDateTime(admin.CreatedAt),
		UpdatedAt: datetime.ConvertUnixToDateTime(admin.UpdatedAt),
	}
}
