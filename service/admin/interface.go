package admin

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/admin/inout"
)

type Service interface {
	Create(ctx context.Context, input *inout.AdminCreateInput) (ID string, err error)
	Read(ctx context.Context, input *inout.AdminReadInput) (admin *inout.AdminView, err error)
	List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.AdminView, err error)
	Delete(ctx context.Context, input *inout.AdminDeleteInput) (err error)
	Update(ctx context.Context, input *inout.AdminUpdateInput) (err error)
}
