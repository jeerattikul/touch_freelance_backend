package wrapper

import (
	"context"

	"go-template-training/service/admin/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Create(ctx context.Context, input *inout.AdminCreateInput) (id string, err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.admin.Create")
	defer sp.Finish()

	sp.LogKV("UserName", input.UserName)
	sp.LogKV("PassWord", input.PassWord)
	sp.LogKV("FirstName", input.FirstName)
	sp.LogKV("LastName", input.LastName)

	id, err = wrp.Service.Create(ctx, input)

	sp.LogKV("ID", id)
	sp.LogKV("Error", err)

	return id, err
}
