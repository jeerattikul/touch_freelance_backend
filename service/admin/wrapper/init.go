package wrapper

import (
	"go-template-training/service/admin"
)

type Wrapper struct {
	Service admin.Service
}

func _(service admin.Service) admin.Service {
	return &Wrapper{service}
}
