package wrapper

import (
	"context"

	"go-template-training/service/admin/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Update(ctx context.Context, input *inout.AdminUpdateInput) (err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.admin.Update")
	defer sp.Finish()

	sp.LogKV("UserName", input.UserName)
	sp.LogKV("PassWord", input.PassWord)
	sp.LogKV("FirstName", input.FirstName)
	sp.LogKV("LastName", input.LastName)

	err = wrp.Service.Update(ctx, input)

	sp.LogKV("Error", err)

	return err
}
