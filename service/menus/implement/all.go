package implement

import (
	"context"
	"fmt"
	"log"

	"go-template-training/domain"
	"go-template-training/service/menus/inout"
	"go-template-training/service/util"
)

func (impl *implementation) All(ctx context.Context) (total int, items []*inout.MenuView, err error) {

	filters := []string{
		fmt.Sprintf("status:eq:%s", "active"),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	sorts := []string{
		"sort:asc",
	}

	opt := &domain.QueryOption{
		Filters: filters,
		Search:  nil,
		Sorts:   sorts,
	}
	total, records, err := impl.Repo.Find(ctx, opt, &domain.Menu{})
	if err != nil {
		log.Println(err)
		return 0, nil, util.RepoListErr(err)
	}

	items = make([]*inout.MenuView, len(records))
	for i, record := range records {
		rec := record.(*domain.Menu)

		items[i] = inout.MenuToView(rec, impl.DateTime)
	}

	return total, items, nil
}
