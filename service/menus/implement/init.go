package implement

import (
	menu "go-template-training/service/menus"
	wrp "go-template-training/service/menus/wrapper"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	"go-template-training/service/validator"
)

const (
	Depa = "depa"
)

type implementation struct {
	*MenusServiceConfig
}

type MenusServiceConfig struct {
	Validator    validator.Validator
	Repo         util.Repository
	UUID         util.UUID
	DateTime     util.DateTime
	FilterString util.FilterString
	Log          logs.Log
}

func New(config *MenusServiceConfig) (service menu.Service) {
	return &wrp.Wrapper{
		Service: &implementation{config},
	}
}
