package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"
)

type MenuView struct {
	ID        string `json:"id" example:"123456789012345678"`
	KeySlug   string `json:"key_slug" example:"key_slug"`
	Name      string `json:"name" example:"John Smith"`
	Icon      string `json:"icon" example:"some_icon"`
	Sort      int    `json:"sort" example:"1"`
	Status    string `json:"status" example:"active"`
	CreatedAt string `json:"created_at" example:"2016-01-02 15:04:05"`
	UpdatedAt string `json:"updated_at" example:"2016-01-02 15:04:05"`
} // @name MenuView

func MenuToView(menu *domain.Menu, datetime util.DateTime) (view *MenuView) {
	return &MenuView{
		ID:        menu.ID,
		KeySlug:   menu.KeySlug,
		Name:      menu.Name,
		Icon:      menu.Icon,
		Sort:      menu.Sort,
		Status:    menu.Status,
		CreatedAt: datetime.ConvertUnixToDateTime(menu.CreatedAt),
		UpdatedAt: datetime.ConvertUnixToDateTime(menu.UpdatedAt),
	}
}
