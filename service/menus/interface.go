package menus

import (
	"context"

	"go-template-training/service/menus/inout"
)

type Service interface {
	All(ctx context.Context) (total int, items []*inout.MenuView, err error)
}
