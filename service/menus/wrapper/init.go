package wrapper

import (
	menu "go-template-training/service/menus"
)

type Wrapper struct {
	Service menu.Service
}

func _(service menu.Service) menu.Service {
	return &Wrapper{service}
}
