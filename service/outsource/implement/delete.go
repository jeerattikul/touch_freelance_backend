package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/outsource/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Delete(ctx context.Context, input *inout.OutsourceDeleteInput) (err error) {
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoOutsource.Read(ctx, filters, &domain.Outsource{}); err != nil {
		return util.RepoReadErr(err)
	}

	if err = impl.RepoOutsource.SoftDelete(ctx, filters); err != nil {
		return util.RepoDeleteErr(err)
	}

	return nil
}
