package implement

import (
	"go-template-training/service/outsource"
	wrp "go-template-training/service/outsource/wrapper"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	"go-template-training/service/validator"
)

type implementation struct {
	*OutsourceServiceConfig
}

type OutsourceServiceConfig struct {
	Validator     validator.Validator
	RepoOutsource util.Repository
	RepoRedis     util.RepositoryRedis
	UUID          util.UUID
	DateTime      util.DateTime
	FilterString  util.FilterString
	Log           logs.Log
}

func New(config *OutsourceServiceConfig) (service outsource.Service) {
	return &wrp.Wrapper{
		Service: &implementation{config},
	}
}
