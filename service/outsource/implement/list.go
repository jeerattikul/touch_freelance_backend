package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/outsource/inout"
	"go-template-training/service/util"
)

func (impl *implementation) List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.OutsourceView, err error) {
	if err = impl.Validator.Validate(opt); err != nil {
		return 0, nil, util.ValidationParamOptionErr(err)
	}

	total, records, err := impl.RepoOutsource.List(ctx, opt, &domain.Outsource{})
	if err != nil {
		return 0, nil, util.RepoListErr(err)
	}

	items = make([]*inout.OutsourceView, len(records))
	for i, record := range records {
		items[i] = inout.OutsourceToView(record.(*domain.Outsource), impl.DateTime)
	}

	return total, items, nil
}
