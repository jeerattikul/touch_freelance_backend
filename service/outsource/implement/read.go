package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/outsource/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Read(ctx context.Context, input *inout.OutsourceReadInput) (view *inout.OutsourceView, err error) {
	outsource := &domain.Outsource{}
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoOutsource.Read(ctx, filters, outsource); err != nil {
		return nil, util.RepoReadErr(err)
	}

	return inout.OutsourceToView(outsource, impl.DateTime), nil
}
