package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/outsource/inout"
	"go-template-training/service/util"

	"github.com/imdario/mergo"
)

func (impl *implementation) Update(ctx context.Context, input *inout.OutsourceUpdateInput) (err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return util.ValidationUpdateErr(err)
	}

	outsource := &domain.Outsource{}
	filters := impl.FilterString.MakeIDFilters(input.ID)

	if err = impl.RepoOutsource.Read(ctx, filters, outsource); err != nil {
		return util.RepoReadErr(err)
	}

	update := input.ToDomain(impl.DateTime)

	if err = mergo.Merge(outsource, update, mergo.WithOverride); err != nil {
		return util.UnknownErr(err)
	}

	if err = impl.RepoOutsource.Update(ctx, filters, outsource); err != nil {
		return util.RepoUpdateErr(err)
	}

	return nil
}
