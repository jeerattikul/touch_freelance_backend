package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"

	"github.com/modern-go/reflect2"
)

type OutsourceCreateInput struct {
	ID           string   `json:"id" swaggerignore:"true"`
	OutName      string   `json:"out_name" validate:"required,not-empty" example:"จีรัฐติกุล แสนละเอียด"`
	Company      string   `json:"company" example:"ทัช เทคโนโลยี" validate:"not-empty"`
	Address      string   `json:"address" example:"203 ซอบมหายชัย2 ต.ในเมือง อ.เมือง จ.นครราชสีมา 30000"`
	OutPhone     []string `json:"out_phone" validate:"not-empty" example:"0996780877,0885458785,0123456789"`
	OutFramework []string `json:"out_framework" validate:"not-empty,name-en" example:"React"`
	OutStatus    string   `json:"out_status" validate:"not-empty" example:"activate"`
	Note         string   `json:"note" example:""`
	Project      []string `json:"project" example:"ระบบจัดการร้านอาหาร,ระบบจัดการคิวหมอฟัน"`
} // @Name OutsourceCreateInput

func (input *OutsourceCreateInput) ToDomain(datetime util.DateTime) (outsource *domain.Outsource) {
	if reflect2.IsNil(input) {
		return &domain.Outsource{}
	}
	return &domain.Outsource{
		ID:           input.ID,
		OutName:      input.OutName,
		Company:      input.Company,
		Address:      input.Address,
		OutPhone:     input.OutPhone,
		OutFramework: input.OutFramework,
		OutStatus:    input.OutStatus,
		Note:         input.Note,
		Project:      input.Project,
		CreatedAt:    datetime.GetUnixNow(),
		UpdatedAt:    datetime.GetUnixNow(),
	}
}
