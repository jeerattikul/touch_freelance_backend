package inout

type OutsourceDeleteInput struct {
	ID string `json:"-" validate:"required"`
} // @Name OutsourceDeleteInput
