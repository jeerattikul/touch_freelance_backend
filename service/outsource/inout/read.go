package inout

type OutsourceReadInput struct {
	ID string `json:"-" validate:"required"`
} // @Name OutsourceReadInput
