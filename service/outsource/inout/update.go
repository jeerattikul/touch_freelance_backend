package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"

	"github.com/modern-go/reflect2"
)

type OutsourceUpdateInput struct {
	ID           string   `json:"id" swaggerignore:"true"`
	OutName      string   `json:"out_name" validate:"required,not-empty" example:"จีรัฐติกุล แสนละเอียด"`
	Company      string   `json:"company" example:"ทัช เทคโนโลยี" validate:"not-empty"`
	Address      string   `json:"address" validate:"not-empty" example:"306 ซอยมหาชัยื4 ตำบลในเมือง อำเภอเมือง จังหวัดนครราชสีมา 30000"`
	OutPhone     []string `json:"out_phone" validate:"not-empty" example:"0956215987,0895456545"`
	OutFramework []string `json:"out_framework" example:"react,flutter" validate:"not-empty"`
	OutStatus    string   `json:"out_status" example:"activate" validate:"not-empty"`
	Note         string   `json:"note" example:"ทำงานได้ตรงตามกำหนด งานเรียบร้อยดี"`
	Project      []string `json:"project" example:"ระบบซื้อจัดการร้านสะดวกซื้อ,เว็บร้านขายทองโคราช,sport shop mobile app"`
} // @Name OutsourceUpdateInput

func (input *OutsourceUpdateInput) ToDomain(datetime util.DateTime) (outsource *domain.Outsource) {
	if reflect2.IsNil(input) {
		return &domain.Outsource{}
	}
	return &domain.Outsource{
		ID:           input.ID,
		OutName:      input.OutName,
		Company:      input.Company,
		Address:      input.Address,
		OutPhone:     input.OutPhone,
		OutFramework: input.OutFramework,
		OutStatus:    input.OutStatus,
		Note:         input.Note,
		Project:      input.Project,
		UpdatedAt:    datetime.GetUnixNow(),
	}
}
