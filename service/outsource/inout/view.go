package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"
)

type OutsourceView struct {
	ID           string   `json:"id" example:"123456789012345678"`
	OutName      string   `json:"out_name" example:"จีรัฐติกุล แสนละเอียด"`
	Company      string   `json:"company" example:"ทัช เทคโนโลยี"`
	Address      string   `json:"address" example:"203 ซอบมหายชัย2 ต.ในเมือง อ.เมือง จ.นครราชสีมา 30000"`
	OutPhone     []string `json:"out_phone" example:"0996780877,0885458785,0123456789"`
	OutFramework []string `json:"out_framework" example:"React"`
	OutStatus    string   `json:"out_status" example:"activate"`
	Note         string   `json:"note" example:""`
	Project      []string `json:"project" example:"ระบบจัดการร้านอาหาร,ระบบจัดการคิวหมอฟัน"`
	CreatedAt    string   `json:"created_at" example:"2016-01-02 15:04:05"`
	UpdatedAt    string   `json:"updated_at" example:"2016-01-02 15:04:05"`
} // @Name OutsourceView

func OutsourceToView(outsource *domain.Outsource, datetime util.DateTime) (view *OutsourceView) {
	return &OutsourceView{
		ID:           outsource.ID,
		OutName:      outsource.OutName,
		Company:      outsource.Company,
		Address:      outsource.Address,
		OutPhone:     outsource.OutPhone,
		OutFramework: outsource.OutFramework,
		OutStatus:    outsource.OutStatus,
		Note:         outsource.Note,
		Project:      outsource.Project,
		CreatedAt:    datetime.ConvertUnixToDateTime(outsource.CreatedAt),
		UpdatedAt:    datetime.ConvertUnixToDateTime(outsource.UpdatedAt),
	}
}
