package outsource

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/outsource/inout"
)

type Service interface {
	Create(ctx context.Context, input *inout.OutsourceCreateInput) (ID string, err error)
	Read(ctx context.Context, input *inout.OutsourceReadInput) (outsource *inout.OutsourceView, err error)
	List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.OutsourceView, err error)
	Delete(ctx context.Context, input *inout.OutsourceDeleteInput) (err error)
	Update(ctx context.Context, input *inout.OutsourceUpdateInput) (err error)
}
