package wrapper

import (
	"context"

	"go-template-training/service/outsource/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Create(ctx context.Context, input *inout.OutsourceCreateInput) (id string, err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.outsource.Create")
	defer sp.Finish()

	sp.LogKV("OutName", input.OutName)
	sp.LogKV("Company", input.Company)
	sp.LogKV("Address", input.Address)
	sp.LogKV("OutPhone", input.OutPhone)
	sp.LogKV("OutFramework", input.OutFramework)
	sp.LogKV("OutStatus", input.OutStatus)
	sp.LogKV("Note", input.Note)
	sp.LogKV("Project", input.Project)

	id, err = wrp.Service.Create(ctx, input)

	sp.LogKV("ID", id)
	sp.LogKV("Error", err)

	return id, err
}
