package wrapper

import (
	"context"

	"go-template-training/service/outsource/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Delete(ctx context.Context, input *inout.OutsourceDeleteInput) (err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.outsource.Delete")
	defer sp.Finish()

	sp.LogKV("ID", input.ID)

	err = wrp.Service.Delete(ctx, input)

	sp.LogKV("Error", err)

	return err
}
