package wrapper

import "go-template-training/service/outsource"

type Wrapper struct {
	Service outsource.Service
}

func _(service outsource.Service) outsource.Service {
	return &Wrapper{service}
}
