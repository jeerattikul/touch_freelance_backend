package wrapper

import (
	"context"

	"go-template-training/service/outsource/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Update(ctx context.Context, input *inout.OutsourceUpdateInput) (err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.outsource.Update")
	defer sp.Finish()

	sp.LogKV("OutName", input.OutName)
	sp.LogKV("Company", input.Company)
	sp.LogKV("Address", input.Address)
	sp.LogKV("OutPhone", input.OutPhone)
	sp.LogKV("OutFramework", input.OutFramework)
	sp.LogKV("OutStatus", input.OutStatus)
	sp.LogKV("Note", input.Note)
	sp.LogKV("Project", input.Project)

	err = wrp.Service.Update(ctx, input)

	sp.LogKV("Error", err)

	return err
}
