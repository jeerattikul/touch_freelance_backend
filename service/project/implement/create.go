package implement

import (
	"context"

	"go-template-training/service/project/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Create(ctx context.Context, input *inout.ProjectCreateInput) (username string, err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return "", util.ValidationCreateErr(err)
	}

	input.ID = impl.UUID.Generate()
	project := input.ToDomain(impl.DateTime)

	_, err = impl.RepoProject.Create(ctx, project)
	if err != nil {
		return "", util.RepoCreateErr(err)
	}

	return input.ID, nil
}
