package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/project/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Delete(ctx context.Context, input *inout.ProjectDeleteInput) (err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return util.ValidationDeleteErr(err)
	}
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoProject.Read(ctx, filters, &domain.Staff{}); err != nil {
		return util.RepoReadErr(err)
	}

	if err = impl.RepoProject.SoftDelete(ctx, filters); err != nil {
		return util.RepoDeleteErr(err)
	}

	return nil
}
