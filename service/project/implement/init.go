package implement

import (
	"go-template-training/service/project"
	wrp "go-template-training/service/project/wrapper"
	"go-template-training/service/util"
	"go-template-training/service/util/logs"
	"go-template-training/service/validator"
)

type implementation struct {
	*ProjectServiceConfig
}

type ProjectServiceConfig struct {
	Validator    validator.Validator
	RepoProject  util.Repository
	RepoRedis    util.RepositoryRedis
	UUID         util.UUID
	DateTime     util.DateTime
	FilterString util.FilterString
	Log          logs.Log
}

func New(config *ProjectServiceConfig) (service project.Service) {
	return &wrp.Wrapper{
		Service: &implementation{config},
	}
}
