package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/project/inout"
	"go-template-training/service/util"
)

func (impl *implementation) List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.ProjectView, err error) {
	if err = impl.Validator.Validate(opt); err != nil {
		return 0, nil, util.ValidationParamOptionErr(err)
	}

	total, records, err := impl.RepoProject.List(ctx, opt, &domain.Project{})
	if err != nil {
		return 0, nil, util.RepoListErr(err)
	}

	items = make([]*inout.ProjectView, len(records))
	for i, record := range records {
		items[i] = inout.ProjectToView(record.(*domain.Project), impl.DateTime)
	}

	return total, items, nil
}
