package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/project/inout"
	"go-template-training/service/util"
)

func (impl *implementation) Read(ctx context.Context, input *inout.ProjectReadInput) (view *inout.ProjectView, err error) {
	project := &domain.Project{}
	filters := []string{
		impl.FilterString.MakeID(input.ID),
		impl.FilterString.MakeDeletedAtIsNull(),
	}

	if err = impl.RepoProject.Read(ctx, filters, project); err != nil {
		return nil, util.RepoReadErr(err)
	}

	return inout.ProjectToView(project, impl.DateTime), nil
}
