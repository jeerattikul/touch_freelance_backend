package implement

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/project/inout"
	"go-template-training/service/util"

	"github.com/imdario/mergo"
)

func (impl *implementation) Update(ctx context.Context, input *inout.ProjectUpdateInput) (err error) {
	if err = impl.Validator.Validate(input); err != nil {
		return util.ValidationUpdateErr(err)
	}

	project := &domain.Project{}
	filters := impl.FilterString.MakeIDFilters(input.ID)

	if err = impl.RepoProject.Read(ctx, filters, project); err != nil {
		return util.RepoReadErr(err)
	}

	update := input.ToDomain(impl.DateTime)

	if err = mergo.Merge(project, update, mergo.WithOverride); err != nil {
		return util.UnknownErr(err)
	}

	if err = impl.RepoProject.Update(ctx, filters, project); err != nil {
		return util.RepoUpdateErr(err)
	}

	return nil
}
