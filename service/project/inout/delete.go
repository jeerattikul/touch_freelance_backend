package inout

type ProjectDeleteInput struct {
	ID      string `json:"-" validate:"required"`
	OutName string `json:"-" validate:"required"`
} // @Name ProjectDeleteInput
