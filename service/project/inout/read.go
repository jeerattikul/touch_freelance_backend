package inout

type ProjectReadInput struct {
	ID string `json:"-" validate:"required"`
} // @Name ProjectReadInput
