package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"

	"github.com/modern-go/reflect2"
)

type ProjectUpdateInput struct {
	ID          string   `json:"id" swaggerignore:"true"`
	ProName     string   `json:"pro_name" validate:"required,not-empty" example:"freeland"`
	Description string   `json:"Descrip_tion" example:" i like thailand "`
	OutName     string   `json:"out_name" example:"jeerattikul"`
	Price       string   `json:"price" example:"7500"`
	SoftDel     string   `json:"soft_del" example:"softdelete"`
	PerRiod     string   `json:"per_riod" example:"20 day"`
	FrameWork   []string `json:"frame_work" example:"react,flutter,python,php"`
	ProStatus   string   `json:"pro_status" example:"close"`
	LinkFile    []string `json:"link_file" example:"https://gitlab.com/jeerattikul/touch_freelance_backend"`
	Tel         string   `json:"tel" example:"0972966061"`
} // @Name ProjectUpdateInput

func (input *ProjectUpdateInput) ToDomain(datetime util.DateTime) (project *domain.Project) {
	if reflect2.IsNil(input) {
		return &domain.Project{}
	}
	return &domain.Project{
		ProName:     input.ProName,
		Description: input.Description,
		OutName:     input.OutName,
		Price:       input.Price,
		SoftDel:     input.SoftDel,
		PerRiod:     input.PerRiod,
		FrameWork:   input.FrameWork,
		ProStatus:   input.ProStatus,
		LinkFile:    input.LinkFile,
		Tel:         input.Tel,
		CreatedAt:   datetime.GetUnixNow(),
		UpdatedAt:   datetime.GetUnixNow(),
	}
}
