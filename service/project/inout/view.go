package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"
)

type ProjectView struct {
	ID          string   `json:"id" swaggerignore:"true"`
	ProName     string   `json:"pro_name" validate:"required,not-empty" example:"freeland"`
	Description string   `json:"Descrip_tion" example:" i like thailand "`
	OutName     string   `json:"out_name" example:"jeerattikul"`
	Price       string   `json:"price" example:"7500"`
	SoftDel     string   `json:"soft_del" example:"1"`
	PerRiod     string   `json:"per_riod" example:"20"`
	FrameWork   []string `json:"frame_work" example:"react,flutter,python,php"`
	ProStatus   string   `json:"pro_status" example:"close"`
	LinkFile    []string `json:"link_file" example:"https://gitlab.com/jeerattikul/touch_freelance_backend"`
	Tel         string   `json:"tel" example:"0972966061"`
	CreatedAt   string   `bson:"created_at"`
	UpdatedAt   string   `bson:"updated_at"`
	DeletedAt   string   `bson:"deleted_at,omitempty"`
} // @Name ProjectView

func ProjectToView(project *domain.Project, datetime util.DateTime) (view *ProjectView) {
	return &ProjectView{
		ID:          project.ID,
		ProName:     project.ProName,
		Description: project.Description,
		OutName:     project.OutName,
		Price:       project.Price,
		SoftDel:     project.SoftDel,
		PerRiod:     project.PerRiod,
		FrameWork:   project.FrameWork,
		ProStatus:   project.ProStatus,
		LinkFile:    project.LinkFile,
		Tel:         project.Tel,
		CreatedAt:   datetime.ConvertUnixToDateTime(project.CreatedAt),
		UpdatedAt:   datetime.ConvertUnixToDateTime(project.UpdatedAt),
	}
}
