package project

import (
	"context"

	"go-template-training/domain"
	"go-template-training/service/project/inout"
)

type Service interface {
	Create(ctx context.Context, input *inout.ProjectCreateInput) (ID string, err error)
	Read(ctx context.Context, input *inout.ProjectReadInput) (project *inout.ProjectView, err error)
	List(ctx context.Context, opt *domain.PageOption) (total int, items []*inout.ProjectView, err error)
	Delete(ctx context.Context, input *inout.ProjectDeleteInput) (err error)
	Update(ctx context.Context, input *inout.ProjectUpdateInput) (err error)
}
