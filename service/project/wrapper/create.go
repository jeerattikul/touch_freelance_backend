package wrapper

import (
	"context"

	"go-template-training/service/project/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Create(ctx context.Context, input *inout.ProjectCreateInput) (id string, err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.project.Create")
	defer sp.Finish()

	sp.LogKV("ProName", input.ProName)
	sp.LogKV("Description", input.Description)
	sp.LogKV("OutName", input.OutName)
	sp.LogKV("Price", input.Price)
	sp.LogKV("SoftDel", input.SoftDel)
	sp.LogKV("PerRiod", input.PerRiod)
	sp.LogKV("FrameWork", input.FrameWork)
	sp.LogKV("ProStatus", input.ProStatus)
	sp.LogKV("LinkFile", input.LinkFile)
	sp.LogKV("tel", input.Tel)

	id, err = wrp.Service.Create(ctx, input)

	sp.LogKV("ID", id)
	sp.LogKV("Error", err)

	return id, err
}
