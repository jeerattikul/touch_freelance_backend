package wrapper

import (
	"go-template-training/service/project"
)

type Wrapper struct {
	Service project.Service
}

func _(service project.Service) project.Service {
	return &Wrapper{service}
}
