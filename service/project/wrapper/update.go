package wrapper

import (
	"context"

	"go-template-training/service/project/inout"

	"github.com/opentracing/opentracing-go"
)

func (wrp *Wrapper) Update(ctx context.Context, input *inout.ProjectUpdateInput) (err error) {
	sp, ctx := opentracing.StartSpanFromContext(ctx, "service.project.Update")
	defer sp.Finish()

	sp.LogKV("ID", input.ID)
	sp.LogKV("ProName", input.ProName)
	sp.LogKV("Description", input.Description)
	sp.LogKV("OutName", input.OutName)
	sp.LogKV("Price", input.Price)
	sp.LogKV("SoftDel", input.SoftDel)
	sp.LogKV("PerRiod", input.PerRiod)
	sp.LogKV("FrameWork", input.FrameWork)
	sp.LogKV("ProStatus", input.ProStatus)
	sp.LogKV("LinkFile", input.LinkFile)
	sp.LogKV("tel", input.Tel)

	err = wrp.Service.Update(ctx, input)

	sp.LogKV("Error", err)

	return err
}
