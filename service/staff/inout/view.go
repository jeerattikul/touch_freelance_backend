package inout

import (
	"go-template-training/domain"
	"go-template-training/service/util"
)

type StaffView struct {
	ID        string `json:"id" example:"123456789012345678"`
	Name      string `json:"name" example:"John Smith"`
	Tel       string `json:"tel" example:"0900000000"`
	CreatedAt string `json:"created_at" example:"2016-01-02 15:04:05"`
	UpdatedAt string `json:"updated_at" example:"2016-01-02 15:04:05"`
} // @Name StaffView

func StaffToView(staff *domain.Staff, datetime util.DateTime) (view *StaffView) {
	return &StaffView{
		ID:        staff.ID,
		Name:      staff.Name,
		Tel:       staff.Tel,
		CreatedAt: datetime.ConvertUnixToDateTime(staff.CreatedAt),
		UpdatedAt: datetime.ConvertUnixToDateTime(staff.UpdatedAt),
	}
}
