package util

import (
	"fmt"
)

//go:generate mockery --name=FilterString
type FilterString interface {
	MakeIDFilters(id string) (filters []string)
	MakeID(id string) (filter string)
	MakeNotEqualID(id string) (filter string)
	MakeDeletedAtIsNull() (filter string)
}

type filter struct{}

func NewFilterString() (filterString FilterString) {
	return &filter{}
}

func (f *filter) MakeIDFilters(id string) (filters []string) {
	return []string{
		fmt.Sprintf("_id:eq:%s", id),
	}
}

func (f *filter) MakeID(id string) (filter string) {
	return fmt.Sprintf("_id:eq:%s", id)
}

func (f *filter) MakeNotEqualID(id string) (filter string) {
	return fmt.Sprintf("_id:ne:%s", id)
}

func (f *filter) MakeDeletedAtIsNull() (filter string) {
	return "deleted_at:isNull"
}
