package admin

import (
	"context"

	"go-template-training/service/admin/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateAdmin) CreateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.AdminCreateInput)

	c.checkUserNameUnique(ctx, structLV, input.UserName)

}
