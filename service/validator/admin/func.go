package admin

import (
	"context"
	"fmt"

	"go-template-training/domain"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateAdmin) checkUserNameUnique(ctx context.Context, structLV validator.StructLevel, user_name string) {
	filters := []string{
		fmt.Sprintf("user_name:eq:%s", user_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Admin{}); err == nil {
		structLV.ReportError(user_name, "UserName", "user_name", "unique", "")
	}
}

func (c *customValidateAdmin) checkUpdateUserNameUnique(ctx context.Context, structLV validator.StructLevel, id string, user_name string) {
	filters := []string{
		fmt.Sprintf("_id:ne:%s", id),
		fmt.Sprintf("user_name:eq:%s", user_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Admin{}); err == nil {
		structLV.ReportError(user_name, "UserName", "user_name", "unique", "")
	}
}
