package admin

import (
	"go-template-training/service/util"
)

type customValidateAdmin struct {
	repo util.Repository
}

func New(repo util.Repository) (customValidate *customValidateAdmin) {
	return &customValidateAdmin{
		repo: repo,
	}
}
