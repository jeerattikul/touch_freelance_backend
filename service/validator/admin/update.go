package admin

import (
	"context"

	"go-template-training/service/admin/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateAdmin) UpdateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.AdminUpdateInput)

	c.checkUpdateUserNameUnique(ctx, structLV, input.ID, input.UserName)
}
