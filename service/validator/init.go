package validator

import (
	ioAdmin "go-template-training/service/admin/inout"
	ioOutsource "go-template-training/service/outsource/inout"
	ioProject "go-template-training/service/project/inout"
	ioStaff "go-template-training/service/staff/inout"

	"go-template-training/service/util"

	vAdmin "go-template-training/service/validator/admin"
	vOutsource "go-template-training/service/validator/outsource"
	vProject "go-template-training/service/validator/project"
	vStaff "go-template-training/service/validator/staff"

	"github.com/go-playground/validator/v10"
)

type GoPlayGroundValidator struct {
	validate *validator.Validate
	Repo     *ValidatorRepository
}

type ValidatorRepository struct {
	Staff     util.Repository
	Admin     util.Repository
	Project   util.Repository
	Outsource util.Repository
}

func New(vRepo *ValidatorRepository) (v *GoPlayGroundValidator) {
	v = &GoPlayGroundValidator{
		validate: validator.New(),
		Repo:     vRepo,
	}

	_ = v.validate.RegisterValidation("not-empty", notEmptyStringFunc)
	_ = v.validate.RegisterValidation("name-th", nameThFunc)
	_ = v.validate.RegisterValidation("name-en", nameEnFunc)

	// custom validate staff
	cStaff := vStaff.New(vRepo.Staff)
	v.validate.RegisterStructValidation(cStaff.CreateStructLevelValidation, &ioStaff.StaffCreateInput{})

	// custom validate admin
	cAdmin := vAdmin.New(vRepo.Admin)
	v.validate.RegisterStructValidation(cAdmin.CreateStructLevelValidation, &ioAdmin.AdminCreateInput{})
	v.validate.RegisterStructValidation(cAdmin.UpdateStructLevelValidation, &ioAdmin.AdminUpdateInput{})

	// custom validate project
	cProject := vProject.New(vRepo.Project)
	v.validate.RegisterStructValidation(cProject.CreateStructLevelValidation, &ioProject.ProjectCreateInput{})
	v.validate.RegisterStructValidation(cProject.UpdateStructLevelValidation, &ioProject.ProjectUpdateInput{})
	v.validate.RegisterStructValidation(cProject.DeleteStructLevelValidation, &ioProject.ProjectDeleteInput{})

	// custom validate outsource
	cOutsource := vOutsource.New(vRepo.Outsource)
	v.validate.RegisterStructValidation(cOutsource.CreateStructLevelValidation, &ioOutsource.OutsourceCreateInput{})
	//v.validate.RegisterStructValidation(cOutsource.UpdateStructLevelValidation, &ioOutsource.OutsourceUpdateInput{})

	return v
}

func (v *GoPlayGroundValidator) Validate(item interface{}) (err error) {
	return v.validate.Struct(item)
}
