package outsource

import (
	"context"

	"go-template-training/service/outsource/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateOutsource) CreateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.OutsourceCreateInput)

	c.checkOutNameUnique(ctx, structLV, input.OutName)

}
