package outsource

import (
	"context"
	"fmt"

	"go-template-training/domain"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateOutsource) checkOutNameUnique(ctx context.Context, structLV validator.StructLevel, out_name string) {
	filters := []string{
		fmt.Sprintf("out_name:eq:%s", out_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Outsource{}); err == nil {
		structLV.ReportError(out_name, "OutName", "out_name", "unique", "")
	}
}

func (c *customValidateOutsource) checkUpdateOutNameUnique(ctx context.Context, structLV validator.StructLevel, id string, out_name string) {
	filters := []string{
		fmt.Sprintf("_id:ne:%s", id),
		fmt.Sprintf("out_name:eq:%s", out_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Outsource{}); err == nil {
		structLV.ReportError(out_name, "OutName", "out_name", "unique", "")
	}
}
