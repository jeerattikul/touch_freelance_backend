package outsource

import (
	"go-template-training/service/util"
)

type customValidateOutsource struct {
	repo util.Repository
}

func New(repo util.Repository) (customValidate *customValidateOutsource) {
	return &customValidateOutsource{
		repo: repo,
	}
}
