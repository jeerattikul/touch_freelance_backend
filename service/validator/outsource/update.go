package outsource

import (
	"context"

	"go-template-training/service/outsource/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateOutsource) UpdateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.OutsourceUpdateInput)

	c.checkUpdateOutNameUnique(ctx, structLV, input.ID, input.OutName)
}
