package project

import (
	"context"

	"go-template-training/service/project/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateProject) CreateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.ProjectCreateInput)

	c.checkProNameUnique(ctx, structLV, input.ProName)

}
