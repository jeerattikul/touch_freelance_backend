package project

import (
	"context"

	"go-template-training/service/project/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateProject) DeleteStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.ProjectDeleteInput)

	//c.checkIDUnique(ctx, structLV, input.ID)
	c.checkOutName(ctx, structLV, input.OutName, input.ID)
	//สามารถลบ Project ได้เป็นการลบแบบ Soft delete เงื่อนไขคือใน Project จะต้องไม่มี Outsource ที่รับผิดชอบ Project อยู่
}
