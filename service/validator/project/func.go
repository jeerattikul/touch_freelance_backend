package project

import (
	"context"
	"fmt"

	"go-template-training/domain"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateProject) checkProNameUnique(ctx context.Context, structLV validator.StructLevel, pro_name string) {
	filters := []string{
		fmt.Sprintf("pro_name:eq:%s", pro_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Project{}); err == nil {
		structLV.ReportError(pro_name, "ProName", "pro_name", "unique", "")
	}
}

func (c *customValidateProject) checkUpdateProNameUnique(ctx context.Context, structLV validator.StructLevel, id string, pro_name string) {
	filters := []string{
		fmt.Sprintf("_id:ne:%s", id),
		fmt.Sprintf("pro_name:eq:%s", pro_name),
		fmt.Sprintf("deleted_at:isNull"),
	}
	if err := c.repo.Read(ctx, filters, &domain.Project{}); err == nil {
		structLV.ReportError(pro_name, "ProName", "pro_name", "unique", "")
	}
}

func (c *customValidateProject) checkOutName(ctx context.Context, structLV validator.StructLevel, outName string, id string) {
	filters := []string{
		fmt.Sprintf("_id:eq:%s", id),
		fmt.Sprintf("out_name:eq:%s", outName),
	}
	if err := c.repo.Read(ctx, filters, &domain.Project{}); err == nil {
		structLV.ReportError("out_name", "out_name", "Project", "can not delete", "")
	}
}
