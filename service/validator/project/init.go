package project

import (
	"go-template-training/service/util"
)

type customValidateProject struct {
	repo util.Repository
}

func New(repo util.Repository) (customValidate *customValidateProject) {
	return &customValidateProject{
		repo: repo,
	}
}
