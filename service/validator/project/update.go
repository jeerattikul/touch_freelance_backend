package project

import (
	"context"

	"go-template-training/service/project/inout"

	"github.com/go-playground/validator/v10"
)

func (c *customValidateProject) UpdateStructLevelValidation(structLV validator.StructLevel) {
	ctx := context.Background()
	input := structLV.Current().Interface().(inout.ProjectUpdateInput)

	c.checkUpdateProNameUnique(ctx, structLV, input.ID, input.ProName)
}
